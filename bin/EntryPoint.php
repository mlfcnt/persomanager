<?php
	include("../config/ConfigParser.php");
	include("../src/CsvFile.php");

	$parser = new ConfigParser();
	$parser->parse('../config/myConfig.txt');
	$files = $parser->readFiles();

	foreach ($files as $file) {
		$file->sort('State');
		$file->write();
	}
?>
